//
//  AppDelegate.swift
//  TestingFastlane
//
//  Created by Tunde on 30/10/2017.
//  Copyright © 2017 Done Brothers (Cash Betting) Ltd. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    return true
  }

}

