fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

## Choose your installation method:

<table width="100%" >
<tr>
<th width="33%"><a href="http://brew.sh">Homebrew</a></td>
<th width="33%">Installer Script</td>
<th width="33%">RubyGems</td>
</tr>
<tr>
<td width="33%" align="center">macOS</td>
<td width="33%" align="center">macOS</td>
<td width="33%" align="center">macOS or Linux with Ruby 2.0.0 or above</td>
</tr>
<tr>
<td width="33%"><code>brew cask install fastlane</code></td>
<td width="33%"><a href="https://download.fastlane.tools">Download the zip file</a>. Then double click on the <code>install</code> script (or run it in a terminal window).</td>
<td width="33%"><code>sudo gem install fastlane -NV</code></td>
</tr>
</table>

# Available Actions
## iOS
### ios test
```
fastlane ios test
```
Build the test branch, increment build number, sync profiles & certificates, submit to hockey and commit it with new build number
### ios uat
```
fastlane ios uat
```
Build the uat branch, sync profiles & certificates and submit the live version to hockey
### ios release
```
fastlane ios release
```
Build the master branch, sync profiles & certificates, create the ipa and send it to the app store and commit it with the release tag
### ios send_to_app_store_with_ipa
```
fastlane ios send_to_app_store_with_ipa
```
Create a new app store submission with the IPA attached
### ios createipa
```
fastlane ios createipa
```
Generate a new IPA file for app store submission
### ios git
```
fastlane ios git
```
Check the correct branch and make sure the repo is clean
### ios commit_build_with_tag
```
fastlane ios commit_build_with_tag
```
Tag the commit with the version and build number and push it to the remote
### ios commit_build
```
fastlane ios commit_build
```
Commit with a new build number in the commit message
### ios generate_certificates
```
fastlane ios generate_certificates
```
Generate and match the certificates and profiles
### ios sumbit_hockey
```
fastlane ios sumbit_hockey
```
Submit the IPA to hockey
### ios send_hockey
```
fastlane ios send_hockey
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
